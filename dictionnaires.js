/* Les differents dictionnaires et la fonction de traduction */
/* Attention: les noms de variables ne doivent pas comporter de tiret ("-") */

function langueDefaut(){
    /*Attention: ne renvoie qu'un string et pas un des objets dictionnaires*/
    /*Ecrire traduire(window[langueDefaut()]) dans document.ready() pour traduire la page dans la langue par défaut du navigateur*/
    languesImplementees = ["fr","en","it","pl","es","pt","ru"];
    langue = navigator.language.toLowerCase();
    langueCode = langue.substring(0, 2);
    if(languesImplementees.indexOf(langueCode) != -1){
        return langueCode;
    }else{return "en";}
}

function traduire(langue) {
    /*Attention: langue doit être un des objets (fr, en,it...) définis ci-après, pas un string*/
    $(".trad").each(function(){
        try{
            $(this).html( langue[$(this).attr('id')] );
        } catch(e) {console.log("Mot non trouvé dans le dictionnaire.");}
    });
}


var fr = {
    titre : "un titre",
    texte : "du texte"
};

var en = {
    titre : "a title",
    texte : "some text"
};
var it = {
    titre : "un titolo",
    texte : "del testo"
};
var pl = {
    titre : "tytuł",
    texte : "tekstu"
};
var es = {
    titre : "un título",
    texte : "del texto"
};
var ru = {
    titre : "название",
    texte : "текста"
};
var pt = {
    titre : "um título",
    texte : "do texto"
};
