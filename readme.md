english below  
italiano sotto  
  
  
**[fr]**  
  
Outil de traduction pour réaliser des pages web multilingues.  
**traduire(langue)**: traduit la page web. *langue* doit être un des tableaux définis plus loin dans le fichier.  
**langueDefaut()**: retourne la langue par défaut du navigateur. Placer le code `traduire(window[langueDefaut()])` pour traduire la page dans la langue par défaut (idéalement dans un *document.ready()*).  
**dictionnaires**: un pour chaque langue, avec comme nom le label standard de la langue. C'est un tableau de correspondance entre un id et le texte qui doit y prendre place:  
`var fr = {
    titre : "un titre",
    texte : "du texte",
    texte2 : "plus de texte"
};`  
**html**: un élément qui doit pouvoir être traduit comporte la classe `trad ` et son id doit avoir une traduction définie dans le dictionnaire.  
  
  
**[en]**  
  
Translation tool to make multilingual web pages.  
**traduire(langue)**: translates the web page. *langue* must be one of the arrays defined further in the file.  
**langueDefaut()**: returns the browser's default language. Place the code `traduire(window[langueDefaut()])` to translate the page to the browser's default language (ideally in a *document.ready()* function).  
**dictionnaires**: one dictionnary for each language, with the standard label of that language as a name. It's a correspondance array between ids and the text that must take place in place:  
`var fr = {
    titre : "un titre",
    texte : "du texte",
    texte2 : "plus de texte"
};`  
**html**: an element that needs to be translated must have the class `trad ` and its id must have a translation defined in the dictionary.  
  
  
**[it]**  
  
Strumento di traduzione per creare pagine web multilingue.  
**traduire(langue)**: traduce la pagina web. *langue* deve essere una delle tabelle definite più avanti nel file.  
**langueDefaut()**: restituisce la lingua predefinita del browser. Imposta il codice `traduire(window[langueDefaut()])` per tradurre la pagina nella lingua predefinita (idealmente in una funzione *document.ready()* ).  
**dictionnaires**: uno per ogni lingua, con l'etichetta standard della lingua come nome. È una tabella di corrispondenza tra un id e il testo che ci deve essere incluso:  
`var fr = {
    titre : "un titre",
    texte : "du texte",
    texte2 : "plus de texte"
};`  
**html**: un elemento che deve essere traducibile deve avere la classe `trad ` e il suo id deve avere una traduzione definita nel dizionario.  
  
